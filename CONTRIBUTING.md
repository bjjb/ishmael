First, join the team. You'll receive a notification when you're a member.

Fork the project at git@gitlab.com/bjjb/bradach.git. Hack away, and create a
merge request.

Merges will be approved if

- they improve the project
- every commit in the changeset represents an incremental improvement
- the motivation behind every commit is explained in its commit message (ideally
  referencing issues for more clarification)
- the pipeline is green for every stage
