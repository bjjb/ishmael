package ishmael

import (
	"context"

	"github.com/docker/docker/api/types"
)

type Network struct {
	types.NetworkResource
}

func Networks() ([]*Network, error) {
	return DefaultClient.Networks()
}

func (c *Client) Networks() ([]*Network, error) {
	list, err := c.NetworkList(context.Background(), types.NetworkListOptions{})
	if err != nil {
		return nil, err
	}
	results := make([]*Network, 0, len(list))
	for _, network := range list {
		details, err := c.NetworkInspect(context.Background(), network.ID)
		if err != nil {
			return nil, err
		}
		results = append(results, &Network{details})
	}
	return results, nil
}
