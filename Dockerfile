FROM golang:alpine AS builder
RUN apk --no-cache add git make
WORKDIR src/gitlab.com/bjjb/ishmael
ADD . .
WORKDIR cmd/ishmael
ENV GOOS=linux
ENV GOARCH=amd64
ENV CGO_ENABLED=0
RUN go build -ldflags '-w -s' -a -installsuffix cgo -o /ishmael
FROM scratch
COPY --from=builder /ishmael /ishmael
EXPOSE 80
CMD ["/ishmael"]
