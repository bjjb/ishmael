package ishmael

import (
	"context"

	"github.com/docker/docker/api/types"
)

type Container struct {
	types.ContainerJSON
}

// Containers gets a list of Containers using the default client
func Containers() ([]*Container, error) {
	return DefaultClient.Containers()
}

// Containers gets a detailed list of Containers
func (c *Client) Containers() ([]*Container, error) {
	list, err := c.ContainerList(context.Background(), types.ContainerListOptions{})
	if err != nil {
		return nil, err
	}
	results := make([]*Container, 0, len(list))
	for _, container := range list {
		details, err := c.ContainerInspect(context.Background(), container.ID)
		if err != nil {
			return nil, err
		}
		results = append(results, &Container{details})
	}

	return results, nil
}
