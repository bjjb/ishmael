.PHONY: build test image

test:
	@ go test ./...

build:
	@ go build ./...

image:
	@ docker build -t bjjb/ishmael .
