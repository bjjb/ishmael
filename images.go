package ishmael

import (
	"context"

	"github.com/docker/docker/api/types"
)

type Image struct {
	types.ImageInspect
}

func Images() ([]*Image, error) {
	return DefaultClient.Images()
}

func (c *Client) Images() ([]*Image, error) {
	list, err := c.ImageList(context.Background(), types.ImageListOptions{})
	if err != nil {
		return nil, err
	}
	results := make([]*Image, 0, len(list))
	for _, i := range list {
		image, _, err := c.ImageInspectWithRaw(context.Background(), i.ID)
		if err != nil {
			return nil, err
		}
		results = append(results, &Image{image})
	}
	return results, nil
}
