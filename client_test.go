package ishmael

import "testing"

func TestDefaultClient(t *testing.T) {
	t.Run("is a client", func(t *testing.T) {
		if DefaultClient == nil {
			t.Errorf("Expected DefaultClient not to be nil")
		}
	})
}
