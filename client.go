package ishmael

import (
	"log"

	"github.com/docker/docker/client"
)

// A Client handles connections to the docker API
type Client struct {
	*client.Client
}

// DefaultClient is the client that gets used for package-level functions
var DefaultClient *Client

func init() {
	client, err := NewClient()
	if err != nil {
		log.Fatalf("failed to create default client: %e", err)
	}
	DefaultClient = client
}

// NewClient gets a new client using the environment (DOCKER_HOST, etc). If
// you need a fancier client, you can build it by hand.
func NewClient() (*Client, error) {
	docker, err := client.NewEnvClient()
	if err != nil {
		return nil, err
	}
	return &Client{docker}, nil
}
