package ishmael

import "github.com/docker/docker/api/types/swarm"

type Swarm struct {
	swarm.Swarm
}

func GetSwarm() (*Swarm, error) {
	return DefaultClient.Swarm()
}

func (c *Client) Swarm() (*Swarm, error) {
	return nil, nil
}
