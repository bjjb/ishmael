package ishmael

import (
	"context"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/filters"
)

type Volume struct {
	*types.Volume
}

// Volumes gets a list of Volumes using the default client
func Volumes() ([]*Volume, []string, error) {
	return DefaultClient.Volumes()
}

// Volumes gets a detailed list of Volumes
func (c *Client) Volumes() ([]*Volume, []string, error) {
	body, err := c.VolumeList(context.Background(), filters.Args{})
	if err != nil {
		return nil, nil, err
	}
	results := make([]*Volume, 0, len(body.Volumes))
	for _, volume := range body.Volumes {
		results = append(results, &Volume{volume})
	}

	return results, body.Warnings, nil
}
