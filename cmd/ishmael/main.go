package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"

	"gitlab.com/bjjb/ishmael"
)

func main() {
	flag.Parse()

	data := make(map[string]interface{})

	containers, err := ishmael.Containers()
	if err != nil {
		log.Fatalf("couldn't inspect containers: %s", err)
	}
	data["containers"] = containers

	networks, err := ishmael.Networks()
	if err != nil {
		log.Fatalf("couldn't inspect networks: %s", err)
	}
	data["networks"] = networks

	volumes, warnings, err := ishmael.Volumes()
	if err != nil {
		log.Fatalf("couldn't inspect volumes: %s", err)
	}
	for _, warning := range warnings {
		log.Printf("warning: %s", warning)
	}
	data["volumes"] = volumes

	images, err := ishmael.Images()
	if err != nil {
		log.Fatalf("couldn't inspect images: %s", err)
	}
	data["images"] = images

	swarm, err := ishmael.GetSwarm()
	if err != nil {
		log.Fatalf("couldn't inspect swarm: %s", err)
	}
	data["swarm"] = swarm

	json, err := json.MarshalIndent(data, "", "  ")
	if err != nil {
		log.Fatalf("couldn't marshal result: %e", err)
	}

	fmt.Print(string(json))
}
